Source: erlang-folsom
Maintainer: Debian Erlang Packagers <pkg-erlang-devel@lists.alioth.debian.org>
Uploaders: Nobuhiro Iwamatsu <iwamatsu@debian.org>, Philipp Huebner <debalance@debian.org>
Section: devel
Priority: optional
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-rebar (>= 0.0.6),
               erlang-bear (>= 0.8.2),
               erlang-meck (>= 0.8.2)
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/erlang-team/packages/erlang-folsom
Vcs-Git: https://salsa.debian.org/erlang-team/packages/erlang-folsom.git
Homepage: https://github.com/boundary/folsom

Package: erlang-folsom
Architecture: all
Depends: ${erlang:Depends},
         ${misc:Depends},
         ${shlibs:Depends},
         erlang-bear (>= 0.8.2),
         erlang-meck (>= 0.8.2)
Enhances: erlang
Description: Erlang based metrics system inspired by Coda Hale's metrics
 Folsom is an Erlang based metrics system inspired by Coda Hale's metrics
 (https://github.com/codahale/metrics/). The metrics API's purpose is to collect
 realtime metrics from your Erlang applications and publish them via Erlang APIs
 and output plugins. folsom is *not* a persistent store. There are 6 types of
 metrics: counters, gauges, histograms (and timers), histories, meter_readers
 and meters. Metrics can be created, read and updated via the `folsom_metrics`
 module.

Package: erlang-folsom-dev
Architecture: all
Depends: ${misc:Depends},
         ${shlibs:Depends},
         erlang-folsom (>= ${source:Version})
Enhances: erlang
Description: development files for erlang-folsom
 Folsom is an Erlang based metrics system inspired by Coda Hale's metrics
 (https://github.com/codahale/metrics/). The metrics API's purpose is to collect
 realtime metrics from your Erlang applications and publish them via Erlang APIs
 and output plugins. folsom is *not* a persistent store. There are 6 types of
 metrics: counters, gauges, histograms (and timers), histories, meter_readers
 and meters. Metrics can be created, read and updated via the `folsom_metrics`
 module.
 .
 This package includes erlang-folsom headers which are necessary to build Erlang
 applications which use erlang-folsom.
